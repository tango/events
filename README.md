events [![Build Status](https://drone.gitea.com/api/badges/tango/events/status.svg)](https://drone.gitea.com/tango/events) [![](http://gocover.io/_badge/gitea.com/tango/events)](http://gocover.io/gitea.com/tango/events)
======

Middleware events is an event middleware for [Tango](https://gitea.com/lunny/tango). 

## Installation

    go get gitea.com/tango/events

## Simple Example

```Go
type EventAction struct {
    tango.Ctx
}

func (c *EventAction) Get() {
    c.Write([]byte("get"))
}

func (c *EventAction) Before() {
    c.Write([]byte("before "))
}

func (c *EventAction) After() {
    c.Write([]byte(" after"))
}

func main() {
    t := tango.Classic()
    t.Use(events.Events())
    t.Get("/", new(EventAction))
    t.Run()
}
```

## License

This project is under BSD License. See the [LICENSE](LICENSE) file for the full license text.